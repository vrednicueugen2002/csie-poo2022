#include <iostream>
#include <string>
#pragma warning(disable:6385)
using namespace std;
static int v[] = { 0, 5, 7, 9, 12, 99, 100 };

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//1. v este un vector global alocat static
//calculati si returnati numarul de elemente al acestui vector

//1. v is a statically allocated global array
//compute and return the number of elements of this array
int numar_elemente() 
{	
	int nrElemente = sizeof(v) / sizeof(v[0]);
	return nrElemente;
}

//2. Functia de mai jos returneaza maximul dintr-un vector
//alocat dinamic primit ca parametru (alaturi de dimensiune)
//daca vectorul este null sau dimensiunea <= 0
//atunci functia returneaza 0

//2. This function should return the max value from a
//dinamically allocated array received as parameter (together with its length)
//if the received array is null or the length <= 0
//the function should return 0
float maxim_vector(float* vector, int dimensiune) 
{
	if (vector == nullptr || dimensiune == 0)
		return 0;
	float max = vector[0];
	for (int i = 1; i < dimensiune; i++)
		if (vector[i] > max)
			max = vector[i];
	return max;
}

//3. Returnati o copie a vectorului de caractere
//primit drept parametru (alocata dinamic)

//3. Return a deep copy of the char array
//received as parameter (dinamically allocated)
char* returnare_copie_vector_caractere(char* sir) 
{
	char* copie = new char[strlen(sir) + 1];
	strcpy_s(copie, strlen(sir) + 1, sir);
	return copie;
}

//4. Functia de mai jos primeste ca si parametri doua stringuri
//returnati lungimea totala a celor doua stringuri

//4. The function bellow receive as parameters two strings
//return the total length of the two strings
int returnare_lungime_stringuri(string s1, string s2) 
{
	return s1.length() + s2.length();
}

//5. Functia de mai jos primeste ca si parametri un string si un char*
//si returneaza true daca cele doua siruri de caractere au acelasi continut

//5. The function bellow receives a string and a char array
//and returns true only of the two strings have the same content
bool siruri_egale(string sir1, char* sir2) 
{
	const char* sir3 = sir1.c_str();
	if (!strcmp(sir3, sir2))
		return true;
	return false;
}

//6. Functia de mai jos primeste ca si parametri o matrice alocata dinamic,
//precum si numarul de linii si de coloane
//functia returneaza true daca matricea primita ca parametru este matrice unitate
//(este patratica, iar pe diagonala principala are doar 1, iar restul elementelor sunt 0)

//6. The function bellow receives a dinamically allocated matrix
//and its dimensions (number of lines and number of columns)
//the function returns true if the matrix is the unit/identity matrix
//(it is a square matrix and on the main diagonal has only 1s, the other elements are 0) 
bool matrice_unitate(int** matrice, int linii, int coloane)
{
	if(linii != coloane)
		return false;
	for(int i = 0; i < linii; i++)
		for (int j = 0; j < coloane; j++)
		{
			if (i == j) {
				if (matrice[i][j] != 1)
					return false;
			}
			else
				if (matrice[i][j] != 0)
					return false;
		}
	return true;
}

//7. Functia de mai jos returneaza un vector ce contine
//minimul de pe fiecare linie al unei matrice primita ca parametru
//vectorul este alocat dinamic si va contine atatea elemente cate linii are matricea

//7. The function bellow return an array that contains
//the minimum from each line of a matrix received as parameter
//the returned array will be a dinamically allocated one and its number of elements
//will be given by the number of lines of the matrix
int* returnare_minim_pe_linii(int** matrice, int linii, int coloane)
{
	if (linii < 1 || coloane < 1)
		return nullptr;
	int* v = new int[linii];
	int dim = 0;
	for (int i = 0; i < linii; i++) {
		int min = matrice[i][0];
		for (int j = 1; j < coloane; j++)
		{
			if (matrice[i][j] < min)
				min = matrice[i][j];
		}
		v[dim++] = min;
	}
	return v;
}

//8. Functia de mai jos este asemanatoare celei de mai sus, doar ca transforma
//elementele vectorului intr-un sir de caractere de tip char* folosind spatii
//ex: daca vectorul returnat mai sus este 1, 2, -2, atunci rezultatul va fi "1 2 -2"

//8. The function bellow is similar to the previous one, but it  transforms
//the output into a char array by using spaces
//eg: if the returned array is 1, 2, -2, then the result will be "1 2 -2"

int nr_cifre(int n)
{
	int cifre = 1;
	n = n / 10;
	while (n)
	{
		cifre++;
		n /= 10;
	}
	return cifre;
}

char* returnare_minim_pe_linii_ca_sir(int** matrice, int linii, int coloane)
{
	int* v = returnare_minim_pe_linii(matrice, linii, coloane);

	char aux[20];
	int nrCifre = 0;
	int negative = 0;

	for (int i = 0; i < linii; i++)
	{
		nrCifre += nr_cifre(v[i]);
		if (v[i] < 0)
			negative++;
	}

	char* sir = new char[nrCifre + negative + linii - 1 + 1]; // negative pentru '-', (linii - 1) pentru ' ', +1 pentru '\0'
	sir[0] = '\0';

	for (int i = 0; i < linii; i++)
	{
		sprintf_s(aux, "%d", v[i]);
		strcat_s(sir, strlen(sir) + strlen(aux) + 1, aux);
		strcat_s(sir,strlen(sir) + 2, " ");
	}
	sir[strlen(sir) - 1] = '\0';
	return sir;
}

//9. Functia de mai jos primeste drept parametri un vector alocat dinamic,
//dimensiunea sa si elementul ce se doreste a fi sters
//si returneaza vectorul obtinut prin stergerea elementului

//9. The function bellow receives a dinamically allocated array,
//its dimension and the element to be deleted
//and returns the array obtained after the deletion of that element
int* sterge_element(int* vector, int dimensiune, int element)
{
	int* v = new int[dimensiune - 1];
	int dim = 0;
	for (int i = 0; i < dimensiune; i++)
	{
		if (vector[i] == element)
			continue;
		v[dim++] = vector[i];
	}
	return v;
}

//10. Functia de mai jos arhiveaza un sir de caractere primit ca parametru
//arhivarea unui sir de caractere se face prin notarea numarului de repetari
//ale unui caracter atunci cand acesta apare de mai mult (sau egal) de 2 ori consecutiv
//ex1: pentru sirul AAABBC, versiunea arhivata este 3A2BC
//ex2: pentru sirul XYYYYYYYZTTT, versiunea arhivata este X7YZ3T
//precizare: sirul contine doar caractere uppercase (litere mari)

//10. The function bellow archives the received char array
//by placing the number of occurences before the character
//when it happens more or equal than twice in a row
//eg1: for the string AAABBC, the archived string should be 3A2BC
//ex2: for the string XYYYYYYYZTTT, the archived string should be X7YZ3T
//remark: the input contains only uppercase letters
char* arhivare_sir(char* sir) 
{
	int litereDiferite = 1;
	int totalCifre = 0;
	if (sir == nullptr || !strcmp(sir, ""))
		return nullptr;
	for (unsigned int i = 0; i < strlen(sir) - 1; i++)
		if (sir[i] != sir[i + 1])
			litereDiferite++;
	int* v = new int[litereDiferite]{0};
	int k = 0;
	v[k]++;
	for (unsigned int i = 1; i < strlen(sir); i++)
	{
		if (sir[i] == sir[i - 1])
			v[k]++;
		else
		{
			if (v[k] > 1)
				totalCifre += nr_cifre(v[k]);
			k++;
			v[k]++;
		}
	}
	char* arhivat = new char[totalCifre + litereDiferite + 1]; // daca sunt mai mult de 10 litere de acelasi fel una dupa alta
	arhivat[0] = '\0';                                         // atunci trebuie +1 caracter la lungimea sirului arhivat
	char aux[20];                                              // am mers pe acelasi principiu si la problema 8 :)
	k = 0;
	for (unsigned int i = 0; i < strlen(sir); i++)
	{
		if (sir[i] != sir[i + 1])
		{
			if (v[k] > 1) {
				sprintf_s(aux, "%d", v[k]);
				strcat_s(arhivat, strlen(arhivat) + strlen(aux) + 1, aux);
			}
			sprintf_s(aux, "%c", sir[i]);
			strcat_s(arhivat, strlen(arhivat) + 2, aux);
			k++;
		}
	}
	return arhivat;
}

int main()
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
}