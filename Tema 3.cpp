#include <iostream>
#include <string>
using namespace std;
bool fisierDeschis = true;

enum tipAplicatie {
	aplicatieWeb = 5,
	aplicatieMobila = 10,
	aplicatieDesktop = 15
};

class Autoturism
{
public:
	string marca;
	string model;
	int putere;
	double pret;
	char numarInmatriculare[10];

	Autoturism()
	{
		marca = "Necunoscuta";
		model = "Necunoscut";
		putere = 0;
		pret = 5000;
	}

	Autoturism(string marca, string model, int putere)
	{
		this->marca = marca;
		this->model = model;
		this->putere = putere;
		this->pret = 5000;
	}

	~Autoturism()
	{
		fisierDeschis = false;
	}

	void discount(int procent)
	{
		if (!(procent < 1 || procent > 50))
			pret = pret - pret * procent / 100;
	}

	void seteazaNumarInmatriculare(char numar[])
	{
		strcpy_s(numarInmatriculare, numar);
	}

	char* obtineNumarInmatriculare()
	{
		return numarInmatriculare;
	}
};

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//1. Functia de mai jos primeste drept parametri doua variabile de tip int
//modificati modalitatea de transmitere a parametrilor astfel incat
//cele doua valori sa fie interschimbate la iesirea din functie
//apelul se va face de forma: inerschimbare(x, y)
//unde x si y sunt 2 intregi

//1. The function below receives two int parameters
//change the way parameteres are passed in such a manner that
//the two values will be interchanged when the function will finish
//the call will look like this: inerschimbare(x, y)
//where x and y are 2 integers
void interschimbare(int& valoare1, int& valoare2) 
{
	int aux = valoare1;
	valoare1 = valoare2;
	valoare2 = aux;
}

//2. Functia de mai jos returneaza o variabila de tipul enumeratie
//(tipAplicatie, enum definit mai sus)
//modificati enumul astfel incat acesta sa aiba trei valori posibile
//Acestea sunt: 5 - aplicatieWeb, 10 - aplicatieMobila, 15 - aplicatieDesktop
//functia primeste drept parametru un string dintre cele de mai sus
//("aplicatieWeb", "aplicatieMobila", etc) si returneaza valoarea din enum
//corespunzatoare ei

//2. The function below return a variable of type enum
//(tipAplicatie, the enum defined above)
//modify the enum so it can have only three posible values
//Those are: 5 - aplicatieWeb, 10 - aplicatieMobila, 15 - aplicatieDesktop
//the function receives one parameter which is a string from the ones above
//("aplicatieWeb", "aplicatieMobila", etc) and returns the enum value
//that corresponds to it
tipAplicatie modificare_enum(string tip) 
{
	if (tip == "aplicatieWeb")
		return tipAplicatie(5);
	if (tip == "aplicatieMobila")
		return tipAplicatie(10);
	return tipAplicatie(15);
}

//3. Modificati constructorul implicit din clasa Autoturism de mai sus
//acesta va initializa marca cu "Necunoscuta", modelul cu "Necunoscut"
//puterea cu 0 si pretul cu 5000

//3. Modify the default constructor of the Autotursim class defined above
//this will initialize marca with "Necunoscuta", modelul with "Necunoscut"
//puterea with 0 and pretul with 5000

//------------------------------------

//4. Modificati constructorul cu 3 parametri: marca, model, putere
//acesta va initializa cele 3 atribute cu valorile primite
//pretul va fi setat la fel ca in celalalt constructor ca fiind 5000

//4. Modify the constructor with 3 parameters: marca, model, putere
//this will initialize the 3 attributes with the received values
//pretul will be set to 5000 as in the other constructor

//------------------------------------

//5. Modificati metoda discount pentru a acorda un discount din pret
//discountul este in procente si poate fi intre 1% si 50%
//functia modifica pretul cu valoarea sa dupa aplicarea discountului

//5. Modify the discount method in order to give a discount from the pret
//the discount is a percentage between 1% and 50%
//the function should modify the price (pret) with the value obtained after
//the discount is applied

//------------------------------------

//6. Adaugati un destructor in clasa care seteaza variabila globala
//fisierDeschis pe false
//6. Add a desctructor to the class that will set the global variable
//fisierDeschis to false

//------------------------------------

//7. Adaugati clasei un atribut nou numit numarInmatriculare
//acesta este un sir de caractere alocat static de lungime maxima 9
//Modificati metodele seteazaNumarInmatriculare si obtineNumarInmatriculare
//astfel incat sa modifice respectiv sa returneze valoarea acestui nou camp

//7. Adad to the class a new attribute called numarInmatriculare
//this is a statically allocated char array with the maximum length 9
//Modify the methods seteazaNumarInmatriculare and obtineNumarInmatriculare
//in such a manner that they will modify (first one) and
//return (second one) the value of this field

//------------------------------------

//8. Functia de mai jos primeste drept parametru un vector static de masini
//reprezentand o flota de autoturisme si numarul sau de elemente
//calculati si returnati valoarea totala a flotei de autoturisme
//prin adunarea preturilor masinilor din flota

//8. The function below receive as arguments an array of cars
//and its number of elements
//compute and return the todal value of the cars (autoturisme/masini)
//by adding the prices (pret) of all the cars from the array
double calcul_valoare_flota(Autoturism masini[], int nr_masini)
{
	double pret = 0;
	for (int i = 0; i < nr_masini; i++)
		pret += masini[i].pret;
	return pret;
}

//9. Functia de mai jos primeste drept parametri un vector alocat dinamic 
//de pointeri la Autoturism impreuna cu numarul de elemente.
//Returnati valoarea celei mai scumpe masini din acel vector

//9. The function below receives as parameters a dinamically allocated
// array of pointers to Autoturism together with its number of elements
//Return the value of the most expensive car from this array
double cea_mai_tare_din_parcare(Autoturism** vector, int nr_masini)
{
	double maxim = 0;
	for (int i = 0; i < nr_masini; i++)
		if (vector[i]->pret > maxim)
			maxim = vector[i]->pret;
	return maxim;
}

//10. Functia de mai jos primeste drept parametri o matrice
//de pointeri la Autoturism impreuna cu numarul de linii si de coloane
//Matricea reprezinta cum sunt dispuse masinile intr-o parcare
//Un element null inseamna ca pe acel loc de parcare nu este nicio masina
//Functia va returna o matrice a locurilor de parcare astfel
//100 90 60
//0 160 75
//0 90 0
//Explicatii: Parcarea are 3 linii si cate 3 locuri de parcare pe fiecare linie
//Pe prima linie sunt parcate 3 masini cu putere de 100, 90 si respectiv 60 cai putere
//pe a doua linie primul loc este neocupat, iar urmatoarele doua locuri
//sunt ocupate de doua masini cu 160, respectiv 75 de cai putere, s.a.m.d.

//10. The function below receives as parameters a matrix of pointers
//to Autoturism together with the number of lines and columns
//The matrix represents the positions of different cars in a parking lot
//A null element means that on that spot there is no parked car
//The function will return a matrix of the parking spots like this
//100 90 60
//0 160 75
//0 90 0
//Explanations: The parkins lot can accomodate cars on 3 lines
//and every line has 3 parking spots
//On the first line there are 3 cars parked with the powers (putere): 100, 90 and 60 HP (horse power)
//on the second line the first spot is free and the next two spots are occupied
//by two cars with 160 and 75 HP, and so on...
int** locuri_libere(Autoturism*** matrice, int nrLinii, int nrColoane) 
{
	int** parcare = new int*[nrLinii];
	for (int i = 0; i < nrLinii; i++)
		parcare[i] = new int[nrColoane];
	for(int i = 0; i < nrLinii; i++)
		for (int j = 0; j < nrColoane; j++)
		{
			if (matrice[i][j] == nullptr)
				parcare[i][j] = 0;
			else
				parcare[i][j] = matrice[i][j]->putere;
		}
	return parcare;
}

int main() 
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
}