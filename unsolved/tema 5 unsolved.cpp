#include <iostream>
#include <string>
using namespace std;

class Cartus
{
private:
	string producator;
	int nrTotalPagini;
};

class Imprimanta
{
private:
	const int serialNumber = 0;
	char* producator;
	int nrPaginiPrintate;
	static string tipImprimanta;

	Imprimanta()
	{
	}

	Imprimanta(int serialNumber, char* producator, int nrPaginiPrintate)
	{
	}

	Imprimanta operator=(Imprimanta i)
	{
		Imprimanta x;
		return x;
	}

	static string getTipImprimanta()
	{
		return "";
	}

	static void setTipImprimanta(string tipImprimanta)
	{
	}

	static long getTotalPaginiPerProducator(Imprimanta* imprimante, int nrImprimante, char* producator)
	{
		return 0;
	}

	void setCartus(Cartus c)
	{
	}

	Cartus getCartus()
	{
		Cartus c;
		return c;
	}

	int getNumarPaginiRamase()
	{
		return 0;
	}
};

string Imprimanta::tipImprimanta = "";

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//--------------------------------------------------------------------------------

//1. Modificati constructorul implicit din clasa Imprimanta de mai sus
//acesta va initializa producatorul cu un sir de caractere vid ("")
//si numarul de pagini printate cu 0

//1. Modify the default constructor of the Imprimanta(Printer) class from above
//this constructor will initialize producator with an empty string ("")
//and nrTotalPagini with 0

//--------------------------------------------------------------------------------

//2. Modificati constructorul cu 3 parametri: serialNumber, producator, nrPaginiPrintate
//acesta va initializa cele 3 atribute cu valorile primite
//atributul constant nu va mai fi initializat la definire
//constructorul implicit va initializa acest atribut cu 1

//2. Modify the constructor with 3 parameters: serialNumber, producator, nrPaginiPrintate
//this is will initialze the attributes with the given values
//the constant field shouldn't be initialized at its declation anymore
//the default constructor will initialize this field with 1

//--------------------------------------------------------------------------------

//3. Adaugati un destructor in clasa pentru a dezaloca
//campul(urile) alocate dinamic

//3. Add a destructor to the class to deallocate
//the dynamically allocated field(s)

//--------------------------------------------------------------------------------

//4. Adaugati clasei un constructor de copiere astfel incat
//sa se realizeze o copie in profunzime (deep copy) a obiectelor
//se va realiza si copierea campului constant

//4. Add a copy constructor to the class
//so it can do a deep copy of the objects
//the copy constructor will copy also the constant field

//--------------------------------------------------------------------------------

//5. Modificati supraincarcarea pentru operator=
//aceasta va realiza o copie in profunzime (deep copy) a obiectelor
//operatorul accepta apeluri in cascada

//5. Modify the overlaoding of the equals operator
//so it can do a deep copy of the objects
//the operator can do chained calls

//--------------------------------------------------------------------------------

//6. Initializati membrul static tipImprimanta cu valoarea "Cerneala"

//6. Initialize the static member tipImprimanta with the value "Cerneala"

//--------------------------------------------------------------------------------

//7. Modificati metodele de acces pentru campul static
//astfel incat acestea sa permita modificarea si obtinerea valorii acestui camp

//7. Modify the getter and the setter for the static field
//so it can return or modify the value of this attribute

//--------------------------------------------------------------------------------

//8. Modificati metoda getTotalPaginiPerProducator pentru a returna numarul total de pagini
//printate de toate imprimantele din vectorul de imprimante primit ca parametru
//care sunt produse de producatorul primit drept parametru
//De ex daca avem 3 imprimante: "Dell" - 120 de pagini, "HP" - 150 de pagini, "Dell" - 170 de pagini
//functia va returna 290 de pagini daca primeste ca parametru producatorul "Dell"
//si 150 de pagini daca primeste ca parametru producatorul "HP"
//pentru orice alt producator functia va returna 0

//8. Modify the method getTotalPaginiPerProducator to return the total number of pages
//printed by all the printers from the given array
//that are produced by the given producer
//E.g. We have 3 printers: "Dell" - 120 pages, "HP" - 150 pages, "Dell" - 170 pages
//the function will return 290 pages if the producator(producer) is "Dell"
//and 150 pages if the producer is "HP"
//for any other producer the function will return 0

//--------------------------------------------------------------------------------

//9. Modelati o relatie de compunere intre clasa Cartus si clasa Imprimanta
//astfel incat sa reiasa faptul ca Imprimanta are (foloseste) un Cartus
//Modificati metoda setCartus astfel incat sa puteti schimba cartusul curent al imprimantei
//cu cel primit drept parametru si metoda getCartus pentru a returna cartusul curent

//9. Model a class composition relation between classes Cartus and Imprimanta
//in such a way that the Imprimanta will have (use) a Cartus
//Modify the method setCartus to be able to change the Cartus
//with the one given a s parameter and the method getCartus to retrun the current value

//--------------------------------------------------------------------------------

//10. Modificati metoda getNumarPaginiRamase astfel incat sa returneze numarul de pagini
//pe care imprimanta respectiva la mai poate printa avand in vedere ca stim 
//cate pagini se pot printa maxim cu cartusul curent si cate pagini au fost printate pana acum
//functia nu poate returna o valoare negativa, asa ca daca numarul curent de pagini il
//depaseste pe cel maxim, va returna 0

//10. Modify the method getNumarPaginiRamase so that it returns the number of pages
//that this printer can print having into consideration that we knoe
//how many pages can be printed with the current cardrige (cartus) and how many pages
//were printed until now
//the function cannot return a negative value, so ff the current number of pages
//is greater than the maximum one, it will return 0

int main() 
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
}