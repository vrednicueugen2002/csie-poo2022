#include <iostream>
#include <string>
using namespace std;
static int v[] = { 0, 5, 7, 9, 12, 99, 100 };

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//1. v este un vector global alocat static
//calculati si returnati numarul de elemente al acestui vector

//1. v is a statically allocated global array
//compute and return the number of elements of this array
int numar_elemente() 
{
	return 0;
}

//2. Functia de mai jos returneaza maximul dintr-un vector
//alocat dinamic primit ca parametru (alaturi de dimensiune)
//daca vectorul este null sau dimensiunea <= 0
//atunci functia returneaza 0

//2. This function should return the max value from a
//dinamically allocated array received as parameter (together with its length)
//if the received array is null or the length <= 0
//the function should return 0
float maxim_vector(float* vector, int dimensiune) 
{
	return 999;
}

//3. Returnati o copie a vectorului de caractere
//primit drept parametru (alocata dinamic)

//3. Return a deep copy of the char array
//received as parameter (dinamically allocated)
char* returnare_copie_vector_caractere(char* sir) 
{
	return nullptr;
}

//4. Functia de mai jos primeste ca si parametri doua stringuri
//returnati lungimea totala a celor doua stringuri

//4. The function bellow receive as parameters two strings
//return the total length of the two strings
int returnare_lungime_stringuri(string s1, string s2) 
{
	return 0;
}

//5. Functia de mai jos primeste ca si parametri un string si un char*
//si returneaza true daca cele doua siruri de caractere au acelasi continut

//5. The function bellow receives a string and a char array
//and returns true only of the two strings have the same content
bool siruri_egale(string sir1, char* sir2) 
{
	return false;
}

//6. Functia de mai jos primeste ca si parametri o matrice alocata dinamic,
//precum si numarul de linii si de coloane
//functia returneaza true daca matricea primita ca parametru este matrice unitate
//(este patratica, iar pe diagonala principala are doar 1, iar restul elementelor sunt 0)

//6. The function bellow receives a dinamically allocated matrix
//and its dimensions (number of lines and number of columns)
//the function returns true if the matrix is the unit/identity matrix
//(it is a square matrix and on the main diagonal has only 1s, the other elements are 0) 
bool matrice_unitate(int** matrice, int linii, int coloane)
{
	return false;
}

//7. Functia de mai jos returneaza un vector ce contine
//minimul de pe fiecare linie al unei matrice primita ca parametru
//vectorul este alocat dinamic si va contine atatea elemente cate linii are matricea

//7. The function bellow return an array that contains
//the minimum from each line of a matrix received as parameter
//the returned array will be a dinamically allocated one and its number of elements
//will be given by the number of lines of the matrix
int* returnare_minim_pe_linii(int** matrice, int linii, int coloane)
{
	return nullptr;
}

//8. Functia de mai jos este asemanatoare celei de mai sus, doar ca transforma
//elementele vectorului intr-un sir de caractere de tip char* folosind spatii
//ex: daca vectorul returnat mai sus este 1, 2, -2, atunci rezultatul va fi "1 2 -2"

//8. The function bellow is similar to the previous one, but it  transforms
//the output into a char array by using spaces
//eg: if the returned array is 1, 2, -2, then the result will be "1 2 -2"
char* returnare_minim_pe_linii_ca_sir(int** matrice, int linii, int coloane)
{
	return nullptr;
}

//9. Functia de mai jos primeste drept parametri un vector alocat dinamic,
//dimensiunea sa si elementul ce se doreste a fi sters
//si returneaza vectorul obtinut prin stergerea elementului

//9. The function bellow receives a dinamically allocated array,
//its dimension and the element to be deleted
//and returns the array obtained after the deletion of that element
int* sterge_element(int* vector, int dimensiune, int element)
{
	return nullptr;
}

//10. Functia de mai jos arhiveaza un sir de caractere primit ca parametru
//arhivarea unui sir de caractere se face prin notarea numarului de repetari
//ale unui caracter atunci cand acesta apare de mai mult (sau egal) de 2 ori consecutiv
//ex1: pentru sirul AAABBC, versiunea arhivata este 3A2BC
//ex2: pentru sirul XYYYYYYYZTTT, versiunea arhivata este X7YZ3T
//precizare: sirul contine doar caractere uppercase (litere mari)

//10. The function bellow archives the received char array
//by placing the number of occurences before the character
//when it happens more or equal than twice in a row
//eg1: for the string AAABBC, the archived string should be 3A2BC
//ex2: for the string XYYYYYYYZTTT, the archived string should be X7YZ3T
//remark: the input contains only uppercase letters
char* arhivare_sir(char* sir) 
{
	return nullptr;
}

int main() 
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
}