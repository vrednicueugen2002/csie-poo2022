#include <iostream>
#include <string>
using namespace std;

class Autoturism
{
private:
	string marca;
	string model;
	int putere;
	double pret;
	float* distanteParcurse;
	//the number of elements for the array above
	int nrDrumuri; //nr de elemente al vectorului de mai sus

public:
	Autoturism()
	{
	}

	Autoturism(string marca, string model, int putere)
	{
	}

	Autoturism(string marca, string model, float* distante, int nrDrumuri)
	{
	}

	string getMarca()
	{
		return "";
	}

	void setMarca(string marca)
	{
	}

	int getNrDrumuri()
	{
		return 0;
	}

	float* getDistanteParcurse()
	{
		return nullptr;
	}

	void setDistante(float* distanteParcurse, int nrDrumuri)
	{
	}

	char* getTaraDeProductie()
	{
		return nullptr;
	}

	void setTaraDeProductie(const char* tara)
	{
	}

	char* AdresaInceputTaraDeProductie()
	{
		return nullptr;
	}
};

//ATENTIE!!!
//Sunt punctate doar solutiile originale si individuale
//Orice incercare de frauda va duce la pierderea intregului punctaj de la seminar
//Signatura functiilor nu se poate modifica, doar continului lor

//WARNING!!!
//Only the original and individual solution will be graded
//Any attempt of copying the solution will lead to loosing the entire lab grade
//The header of the functions cannot be modified, just their body

//---------------------------------------------------------------

//1. Modificati constructorul implicit din clasa Autoturism de mai sus
//acesta va initializa marca cu "Necunoscuta", modelul cu "Necunoscut"
//puterea cu 0, pretul cu 5000, vectorul de distante cu NULL si nrDrumuri cu 0

//1. Modify the default constructor of the Autoturism class from above
//this will initialize marca with "Necunoscuta", model with "Necunoscut"
//putere with 0, pret with 5000, distante with NULL and nrDrumuri with 0

//---------------------------------------------------------------

//2. Modificati constructorul cu 3 parametri: marca, model, putere
//acesta va initializa cele 3 atribute cu valorile primite
//restul de atribute nu trebuie sa ramana neinitializate si vor folosi aceleasi valori implicite ca mai sus

//2. Modify the constructor with 3 parameters: marca, model, putere
//this will initialize the attributes with the given values
//the other attributes shouldn't remain uninitialized and they will use the same implicit values as before

//---------------------------------------------------------------

//3. Modificati constructorul cu 4 parametri pentru a copia valorile
//primite drept parametri in atributele clasei
//constructorul nu va lasa atribute neinitializate

//3. Modify the constructor with 4 parameters to copy the
//received values in the class' attributes
//the constructor will not leave any uninitialized attributes

//---------------------------------------------------------------

//4. Adaugati un destructor in clasa care sterge zonele de memorie anterior alocate
//acesta va seta pointerii pe NULL dupa ce face stergerea

//4. Add a destructor to the class that will deallocate the necessary memory
//this will reset all the pointers to NULL after the deallocation

//---------------------------------------------------------------

//5. Adaugati clasei un constructor de copiere astfel incat
//sa se realizeze o copie in profunzime (deep copy) a obiectelor

//5. Add a copy constructor to the class so
//it can create deep copies of objects

//---------------------------------------------------------------

//6. Modificati metodele de acces (getteri si setteri) pentru atributul marca
//Setterul va accepta denumiri de marci cu o lungime
//mai mare sau egala de 3 caractere
//in caz contrar marca ramane cea existenta

//6. Modify the getters and setters for marca
//The setter will accept only parameters
//with a length greater or equal than 3
//otherwise the value of the attribute should remain unchanged

//---------------------------------------------------------------

//7. Modificati getter-ul pentru vectorul de distante parcurse
//Getter-ul va returna o copie a vectorului din clasa
//Modificati getter-ul si pentru numarul de drumuri

//7. Modify the getter for the distante field
//this will return a copy of the private array
//Modify also the getter for nrDrumuri

//---------------------------------------------------------------

//8. Modificati setter-ul pentru vectorul de distante
//acesta va modifica simultan vectorul si numarul de elemente
//si nu va accepta decat vectori nenuli si numar de elemente mai mare decat 0

//8. Modify the setter for the distante array
//this is simultaniously modify the array and its number of elements
//and will accept only non-null arrays and number of elements greater than 0

//---------------------------------------------------------------

//9. Adaugati un camp de tip char* in clasa numit taraDeProductie
//modificati getter-ul si setter-ul din clasa pentru a returna,
//respectiv a modifica valoarea acestui camp
//Metoda AdresaInceputTaraDeProductie intoarce adresa primului element din tara de productie
//(da, nu e neaparat safe, dar e necesar pentru test)

//9. Add a new char* attribute called taraDeProductie
//modify the existings getters and setters to return
//and modify the value of this field
//The method AdresaInceputTaraDeProductie returns the address of the first element from taraDeProductie
//(yes, this is not safe, but it is needed for the test)

//---------------------------------------------------------------

//10. Modificati constructorii, destructorul si alte metode necesare
//astfel incat clasa sa gestioneze corect noul camp

//10. Modify the constructors, the destructor and all the other methods
//in such a way that the class will correctly manage the new field

int main() 
{
	//Playgroud
	//Testati aici functiile dorite si folositi debugger-ul pentru eventualele probleme
	//Test here the functions that you need and use the debugger to identify the problems
}